#include <vector>
#include <algorithm>
#include <iostream>
#include <cmath>
#include "andrew.h"



bool comparePoints(const Point & a, const Point & b) {
    return (a.x < b.x) || (a.x == b.x && a.y < b.y);
}

// OA x OB 
// ���� � < 0 => ������ �������
double crossProduct(const Point& O, const Point& A, const Point& B) {
    return (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x);
}

std::vector<Point> andrewConvexHull(std::vector<Point>& points) {

    // ���������� ����� �� ���������� x
    sort(points.begin(), points.end());

    std::vector<Point> upper_hull, lower_hull;
    int n = points.size();
    // ���������� ������� ��������
    for (int i = 0; i < n; ++i) {
        while (upper_hull.size() >= 2 && crossProduct(upper_hull[upper_hull.size() - 2], upper_hull.back(), points[i]) < 0) {
            upper_hull.pop_back();
        }
        upper_hull.push_back(points[i]);
    }

    // ���������� ������ ��������
    for (int i = n - 1; i >= 0; --i) {
        while (lower_hull.size() >= 2 && crossProduct(lower_hull[lower_hull.size() - 2], lower_hull.back(), points[i]) < 0) {
            lower_hull.pop_back();
        }
        lower_hull.push_back(points[i]);
    }

    upper_hull.pop_back(); // ������� ��������� ����� ������� ��������, ��� ��� ��� �������� ������� ������
    lower_hull.pop_back(); // ������� ��������� ����� ������ ��������, ��� ��� ��� �������� ������� �������

    upper_hull.insert(upper_hull.end(), lower_hull.begin(), lower_hull.end());

    return upper_hull;
}


bool isPointInPolygon(const std::vector<Point>& polygon, const Point& p) {
    int n = polygon.size();
    bool inside = false;

    for (int i = 0, j = n - 1; i < n; j = i++) {
        if ((polygon[i].y > p.y) != (polygon[j].y > p.y) &&
            (p.x < (polygon[j].x - polygon[i].x) * (p.y - polygon[i].y) / (polygon[j].y - polygon[i].y) + polygon[i].x)) { // x_inter = x_1 + (y - y_1)*(x_2 - x_1) / (y_2 - y_1)
            inside = !inside;
        }
    }

    return inside;
}

bool isPointOnSegment(const Point& a, const Point& b, const Point& p) {
    return std::fabs(crossProduct(a, b, p)) < EPSILON && std::min(a.x, b.x) <= p.x && p.x <= std::max(a.x, b.x) &&
        std::min(a.y, b.y) <= p.y && p.y <= std::max(a.y, b.y);
}

PointLocation pointLocation(const std::vector<Point>& polygon, const Point& p) {
    PointLocation location;
    location.index = -1;

    // ��������, �������� �� ����� ��������
    for (size_t i = 0; i < polygon.size(); ++i) {
        if (std::fabs(polygon[i].x - p.x) < EPSILON && std::fabs(polygon[i].y - p.y) < EPSILON) {
            location.location = Location::Vertex;
            location.index = i;
            return location;
        }
    }

    // ��������, ����� �� ����� �� �������
    for (size_t i = 0, j = polygon.size() - 1; i < polygon.size(); j = i++) {
        if (isPointOnSegment(polygon[i], polygon[j], p)) {
            location.location = Location::OnEdge;
            return location;
        }
    }

    if (isPointInPolygon(polygon, p)) {
        location.location = Location::Inside;
    }
    else {
        location.location = Location::Outside;
    }

    return location;
}

bool inside(Point point, Point edge_start, Point edge_end) {
    return (edge_end.x - edge_start.x) * (point.y - edge_start.y) > (edge_end.y - edge_start.y) * (point.x - edge_start.x);
}

Point intersection(Point edge_start, Point edge_end, Point clip_edge_start, Point clip_edge_end) {
    double dx1 = edge_end.x - edge_start.x;
    double dy1 = edge_end.y - edge_start.y;
    double dx2 = clip_edge_end.x - clip_edge_start.x;
    double dy2 = clip_edge_end.y - clip_edge_start.y;

    double denom = dx1 * dy2 - dy1 * dx2;
    if (denom == 0) {
        return { 0, 0 };
    }

    double t = ((clip_edge_start.x - edge_start.x) * dy2 - (clip_edge_start.y - edge_start.y) * dx2) / denom;
    if (t >= 0 && t <= 1) {
        double ix = edge_start.x + t * dx1;
        double iy = edge_start.y + t * dy1;
        return { ix, iy };
    }
    else {
        return { 0, 0 };  // ����� �� ������������ (�� ������ ������ �����-�� �����)
    }
}
std::vector<Point> sutherland_hodgman(std::vector<Point> subject_polygon, Point clip_edge_start, Point clip_edge_end) {
    std::vector<Point> output_polygon;
    Point s = subject_polygon.back();

    for (auto e : subject_polygon) {
        if (inside(e, clip_edge_start, clip_edge_end)) {
            if (!inside(s, clip_edge_start, clip_edge_end)) {
                Point intersection_point = intersection(s, e, clip_edge_start, clip_edge_end);
                if (intersection_point.x != 0 || intersection_point.y != 0) {
                    output_polygon.push_back(intersection_point);
                }
            }
            output_polygon.push_back(e);
        }
        else if (inside(s, clip_edge_start, clip_edge_end)) {
            Point intersection_point = intersection(s, e, clip_edge_start, clip_edge_end);
            if (intersection_point.x != 0 || intersection_point.y != 0) {
                output_polygon.push_back(intersection_point);
            }
        }
        s = e;
    }

    return output_polygon;
}