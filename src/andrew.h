#ifndef ANDREW_H
#define ANDREW_H

#include <vector>
#define EPSILON 1e-6

struct Point {
    double x, y;
    Point(double _x, double _y) : x(_x), y(_y) {}
    Point() : x(0), y(0) {}
    bool operator<(const Point& other) const {
        return x < other.x || (x == other.x && y < other.y);
    }
};



enum class Location {
    Inside,
    OnEdge,
    Vertex,
    Outside
};

struct PointLocation {
    Location location;
    int index;
};

std::vector<Point> andrewConvexHull(std::vector<Point>& points);
bool isPointInPolygon(std::vector<Point>& polygon, Point p);

std::vector<Point> andrewConvexHull(std::vector<Point>& points);
PointLocation pointLocation(const std::vector<Point>& polygon, const Point& p);

std::vector<Point> sutherland_hodgman(std::vector<Point> subject_polygon, Point clip_edge_start, Point clip_edge_end);
Point intersection(Point edge_start, Point edge_end, Point clip_edge_start, Point clip_edge_end);
bool inside(Point point, Point edge_start, Point edge_end);

#endif