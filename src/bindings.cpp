#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "andrew.h"

namespace py = pybind11;

PYBIND11_MODULE(convex_hull, m) {
    py::class_<Point>(m, "Point")
        .def(py::init<>())
        .def(py::init<double, double>())
        .def_readwrite("x", &Point::x)
        .def_readwrite("y", &Point::y)
        .def("__lt__", &Point::operator<);

    py::enum_<Location>(m, "Location")
        .value("Inside", Location::Inside)
        .value("OnEdge", Location::OnEdge)
        .value("Vertex", Location::Vertex)
        .value("Outside", Location::Outside);

    py::class_<PointLocation>(m, "PointLocation")
        .def_readwrite("location", &PointLocation::location)
        .def_readwrite("index", &PointLocation::index);

    m.def("andrew_convex_hull", &andrewConvexHull, "Compute the convex hull of a set of points using Andrew's algorithm");
    m.def("point_location", &pointLocation, "Determine the location of a point relative to a polygon");
    m.def("sutherland_hodgman", &sutherland_hodgman);
}
