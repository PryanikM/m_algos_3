import numpy as np
import sys
import pytest
import scipy.spatial as ss

sys.path.append('../build/Release/')
import convex_hull

points_x = [11, 22, 13, 10, 5, 31, 9, 21, 3, 63]
points_y = [7, 2, 3, 14, 5, 12, 5, 4, 12, 4]
point_inside = [(10, 11), (60, 4.5), (30, 4)]
point_outside = [(10, 50), (90, 9), (30, 1)]
point_vertex = [(3., 12.), (5., 5.), (3., 12.)]
point_edge = [(4.0, 8.5), (42.5, 3.0), (20.5, 13.0)]

def get_hull(points_x, points_y):
    points = np.column_stack((points_x, points_y)).tolist()
    points = [convex_hull.Point(pt[0], pt[1]) for pt in points]
    return convex_hull.andrew_convex_hull(points)

def get_point_position(test_point, hull):
    location = convex_hull.point_location(hull, test_point)

    location_str = ""
    if location.location == convex_hull.Location.Inside:
        location_str = "Inside"
    elif location.location == convex_hull.Location.OnEdge:
        location_str = "On the Edge"
    elif location.location == convex_hull.Location.Vertex:
        location_str = f"Vertex (index {location.index})"
    else:
        location_str = "Outside"

    return location_str, location


def sets_allclose(set1, set2, rtol=1e-05, atol=1e-08):

    assert len(set1) == len(set2), f"Size set1 = {len(set1)} Size set2 = {len(set2)};  {len(set1)} != {len(set2)}"
    for a in set1:
        assert any(np.isclose(a, b, rtol=rtol, atol=atol) for b in set2)

def run_tests(point_arr, compare, hull):
    for test in point_arr:
        print(F'{test[0]}   {test[1]}')
        point = convex_hull.Point(test[0], test[1])
        location_str, location = get_point_position(point, hull)
        assert location.location == compare, f"Expected {compare}, got {location.location} for point {test}"

@pytest.fixture
def hull():
    return get_hull(points_x, points_y)


@pytest.fixture
def hull_big_scipy_c():
    num_points = 100000
    rng = np.random.default_rng()
    points = rng.random((num_points, 2))

    x = [pt[0] for pt in points]
    y = [pt[1] for pt in points]
    points_c = [convex_hull.Point(pt[0], pt[1]) for pt in points]

    return ss.ConvexHull(points), convex_hull.andrew_convex_hull(points_c), points

@pytest.mark.parametrize("test_point", point_inside)
def test_inside(hull, test_point):
    run_tests([test_point], convex_hull.Location.Inside, hull)

@pytest.mark.parametrize("test_point", point_outside)
def test_outside(hull, test_point):
    run_tests([test_point], convex_hull.Location.Outside, hull)

@pytest.mark.parametrize("test_point", point_vertex)
def test_vertex(hull, test_point):
    run_tests([test_point], convex_hull.Location.Vertex, hull)

@pytest.mark.parametrize("test_point", point_edge)
def test_edge(hull, test_point):
    run_tests([test_point], convex_hull.Location.OnEdge, hull)

def test_with_scipy(hull_big_scipy_c):
    hull_scipy = hull_big_scipy_c[0]
    hull_c = hull_big_scipy_c[1]
    points = hull_big_scipy_c[2]
    x_scipy_res = []
    y_scipy_res = []
    for simplex in hull_scipy.simplices:
        x_scipy_res.append(points[simplex[0], 0])
        x_scipy_res.append(points[simplex[1], 0])

        y_scipy_res.append(points[simplex[0], 1])
        y_scipy_res.append(points[simplex[1], 1])

    x_scipy_res = sorted(set(x_scipy_res))
    y_scipy_res = sorted(set(y_scipy_res))

    x_points_c = [p.x for p in hull_c]
    y_points_c = [p.y for p in hull_c]
    x_points_c = sorted(set(x_points_c))
    y_points_c = sorted(set(y_points_c))
    sets_allclose(x_scipy_res, x_points_c)
    sets_allclose(y_scipy_res, y_points_c)

